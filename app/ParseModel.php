<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParseModel extends Model {

    public static function parseHTML($url) {
        $result = array();
        $htmlPage = file_get_contents($url);
        preg_match_all('/<article class="article">(.*)<\/article>/Uis', $htmlPage, $article); //�������� ������
        preg_match_all('/<header>(.*)<\/header>/Uis', $article[1][0], $title); //���������� ��������
        $result['title'] = strip_tags($title[1][0]);
        $article[1][0] = preg_replace('/<script.*?\/script>/si', '', $article[1][0]); //������� �������
        $article[1][0] = preg_replace('/<div id="jp-relatedposts"(.*)/is', '', $article[1][0]); //������� �������� � �������
        $article[1][0] = preg_replace('/<div class="breadcrumb".*?\/div>/is', '', $article[1][0]); //������� ������� ������
        preg_match_all('/<img.*?>/si', $article[1][0], $images); //����� ��������
        $result['imgAmount'] = count($images[0]);
        preg_match_all('/<video.*?>/si', $article[1][0], $videos); //����� �����
        $result['videoAmount'] = count($videos[0]);
        $tempText = preg_replace('/<\/.*?>/is', ' ', $article[1][0]); //���������� �������� ��� ���������� ����
        for ($i = 1; $i < 4; $i++) {
            $keyName = 'h' . $i;
            $tag = '<' . $keyName . '>';
            preg_match_all($tag, $tempText, $temp);
            $result[$keyName] = count($temp[0]);
        }
        $tempText = strip_tags($tempText); //������� ����
        $tempText = preg_replace('/  +/is', ' ', $tempText); //������� ������� ������� 
        $text = trim($tempText); //������� ������� � ������ � �����
        $result['charsAmount'] = mb_strlen($text);
        $words = explode(" ", $text);
        $result['wordsAmount'] = count($words);
        return $result;
    }

}

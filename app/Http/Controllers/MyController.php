<?php

namespace App\Http\Controllers;

use App;

use Illuminate\Support\Facades\DB;

class MyController extends Controller {

    private $urls = array('http://ishtory.ru/spalnya/zelenye-shtory-v-interere.html',
        'http://ishtory.ru/spalnya/rimskie-shtory-v-spalnyu.html',
        'http://ishtory.ru/zal/shtory-na-dva-okna.html',
        'http://ishtory.ru/zhalyuzi/elektroprivod-dlya-zhalyuzi-svoimi-rukami.html');
    public $data = array();

    public function index() {

        foreach ($this->urls as $url) {
            $result = App\ParseModel::parseHTML($url);
            DB::table('table')->insert($result);
            $this->data[] = $result;
        }
        $list = $this->data;
        return view('index')->with('list', $list);
    }

}

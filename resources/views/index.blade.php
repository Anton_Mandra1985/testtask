<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>table</title>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    </head>
    <body>
        <div class='container'>
        <table class="table table-hover table-bordered">
            <tr>
                <td colspan='4'>Site</td>
                <td colspan='4'>http://ishtory.ru</td>                              
            </tr>
            <tr>
                <th>Article</th>
                <th>H1</th>
                <th>H2</th>
                <th>H3</th>
                <th>Image</th>
                <th>Video</th>
                <th>Chars</th>
                <th>Words</th>
            </tr>
            @foreach ($list as $row)
            <tr>
                <td>{{$row['title']}}</td>
                <td>{{$row['h1']}}</td>
                <td>{{$row['h2']}}</td>
                <td>{{$row['h3']}}</td>
                <td>{{$row['imgAmount']}}</td>
                <td>{{$row['videoAmount']}}</td>
                <td>{{$row['charsAmount']}}</td>
                <td>{{$row['wordsAmount']}}</td>
            </tr>
            @endforeach
        </table>
            </div>
    </body>
</html>
